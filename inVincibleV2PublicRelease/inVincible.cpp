/* 
File:						inVincibleV2PublicRelease.exe
Version:					2.5
State:						Final Release
Language:					C++
Creator:					WasserEsser
Thanks to:					
							- PsychoBitch
							- d366
							- learn_more
							- Geertje123
							- ChrisHD
							- Yazzn
							- morsisko
							- Roskonix
							- pycache
							- HaZeeFFeCt
							- Levak
							- E1oz
Thread:						http://www.unknowncheats.me/forum/counterstrike-global-offensive/152465-invincible-v2-triggerbot-glow-esp-bunnyhop-no-flash-radar-source-code.html
Note:						This is the last Version of inVincibleV2PublicRelease. 
							Feel free to use this for your own learning experience since this has helped me so much. 
							I was able to learn so much throughout the Project inVincible so far. 
							Thanks to the awesome UnknownCheats Community and Spotify.
							I can't wait to release inVincibleV3PublicRelease! 
							But it will take a while ;)

							Thanks.
*/

/* Includes */
#include "inVincible.h"
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <msclr\marshal.h>
#include "ProcMem.h"
#include "OffsetsAndVariables.h"

/* Creating an Object of ProcMem */
ProcMem Mem;

/* Threading Related */
#pragma comment(lib, "user32.lib")

/* Namespaces */
using namespace std;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Threading;
using namespace msclr::interop;

/* Function Prototypes */
void GetKeyInput();
void ReadData();
void GlowESP();
void NoFlash();
void Bunnyhop();
void Triggerbot();
void Radar();
void TriggerShot();
void TriggerBurst();
GlowStruct GetTeamColor();
GlowStruct GetEnemyColor();

/* Defining Colors */
GlowStruct CRed = { 0.4f, 0.f, 0.f, 1.f, true, false };
GlowStruct CGreen = { 0.f, 0.4f, 0.f, 1.f, true, false };
GlowStruct CBlue = { 0.f, 0.f, 0.4f, 1.f, true, false };
GlowStruct CYellow = { 0.4f, 0.4f, 0.f, 1.f, true, false };
GlowStruct COrange = { 1.f, 0.4666f, 0.f, 1.f, true, false };
GlowStruct CPurple = { 0.3647f, 0.f, 1.f, 1.f, true, false };
GlowStruct CWhite = { 1.f, 1.f, 1.f, 1.f, true, false };

/* Array of GlowColors */
GlowStruct GlowColors[7] = { CRed, CGreen, CBlue, CYellow, COrange, CPurple, CWhite };

/* Main Function */
[STAThread]
void Main(array<String^>^ args) 
{
	/* Set Process to use (csgo.exe) */
	Mem.Process("csgo.exe");

	/* Set Module and store it in Client */
	Client = Mem.Module("client.dll");

	/* Define and start Threads for each function */

	ThreadStart ^ t_ReadData = gcnew ThreadStart(&ReadData);
	Thread ^ ReadDataThread = gcnew Thread(t_ReadData);
	ReadDataThread->Start();

	ThreadStart ^ t_GlowESP = gcnew ThreadStart(&GlowESP);
	Thread ^ GlowESPThread = gcnew Thread(t_GlowESP);
	GlowESPThread->Start();

	ThreadStart ^ t_NoFlash = gcnew ThreadStart(&NoFlash);
	Thread ^ NoFlashThread = gcnew Thread(t_NoFlash);
	NoFlashThread->Start();

	ThreadStart ^ t_Bunnyhop = gcnew ThreadStart(&Bunnyhop);
	Thread ^ BunnyhopThread = gcnew Thread(t_Bunnyhop);
	BunnyhopThread->Start();

	ThreadStart ^ t_Triggerbot = gcnew ThreadStart(&Triggerbot);
	Thread ^ TriggerbotThread = gcnew Thread(t_Triggerbot);
	TriggerbotThread->Start();

	ThreadStart ^ t_Radar = gcnew ThreadStart(&Radar);
	Thread ^ RadarThread = gcnew Thread(t_Radar);
	RadarThread->Start();

	ThreadStart ^ t_KeyInput = gcnew ThreadStart(&GetKeyInput);
	Thread ^ KeyInputThread = gcnew Thread(t_KeyInput);
	KeyInputThread->Start();

	/* Display the Graphical User Interface */
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	inVincibleV2PublicRelease::inVincible form;
	Application::Run(%form);

	/* Terminate Threads when the program is exited */

	ReadDataThread->Abort();
	GlowESPThread->Abort();
	NoFlashThread->Abort();
	BunnyhopThread->Abort();
	TriggerbotThread->Abort();
	RadarThread->Abort();
	KeyInputThread->Abort();
}

/* -------------------- */
/* GetKeyInput Function */
/* -------------------- */

void GetKeyInput()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Check if TriggerToggleCheckbox is enabled && TriggerKey is pressed */
		if (TriggerToggleCheckbox && GetAsyncKeyState(TriggerKey) & 1)
			/* If TriggerKey is pressed, toggle the TriggerToggle bool */
			TriggerToggle = !TriggerToggle;

		/* Sleep to reduce CPU usage and to prevent it from toggling it x times a second */
		Sleep(500);
	}
}

/* ----------------- */
/* ReadData Function */
/* ----------------- */

/* Reads non-individual data (now only when necessary) */
void ReadData()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Reads data everytime we go through the loop */

		/* LocalBase */
		LocalBase = Mem.Read<DWORD>(Client + LocalPlayer);

		/* LocalPlayer_Team */
		LocalPlayer_Team = Mem.Read<int>(LocalBase + m_iTeamNum);

		if (GlowEnabled)
			/* GlowPointer */
			GlowPointer = Mem.Read<DWORD>(Client + GlowObjectBase);

		if (TriggerEnabled)
		{
			/* inCross */
			inCross = Mem.Read<int>(LocalBase + m_iCrossHairID);

			/* Trigger_EntityBase */
			Trigger_EntityBase = Mem.Read<DWORD>(Client + EntityList + ((inCross - 1) * 16));

			/* Trigger_EntityBase_TeamID */
			Trigger_EntityBase_TeamID = Mem.Read<int>(Trigger_EntityBase + m_iTeamNum);
		}		

		if (NoFlashEnabled)
			/* FlashMaxAlpha */
			FlashMaxAlpha = Mem.Read<float>(LocalBase + flashMaxAlpha);

		if (BunnyEnabled)
			/* fFlags */
			fFlag = Mem.Read<byte>(LocalBase + m_fFlags);

		/* Sleep to reduce CPU usage */
		Sleep(1);
	}
}

/* ----------------- */
/* Glow ESP Function */
/* ----------------- */

/* What data are we even writing to? */

/*

Mem.Write<float>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0x4)), 0.f);    <--------------- How strong should the RED color be represented?   (Atleast i think that's how it is)		0.f -> 1.f (float)
Mem.Write<float>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0x8)), 0.f);    <--------------- How strong should the GREEN color be represented? (Atleast i think that's how it is)		0.f -> 1.f (float)
Mem.Write<float>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0xC)), 0.f);    <--------------- How strong should the BLUE color be represented?  (Atleast i think that's how it is)		0.f -> 1.f (float)
Mem.Write<float>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0x10)), 1.f);   <--------------- Alpha																					0.f -> 1.f (float)
Mem.Write<bool>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0x24)), true);   <--------------- RenderWhenOccluded																	    true/false (bool)
Mem.Write<bool>((GlowPointer + ((g_currentPlayer_GlowIndex * 0x34) + 0x25)), false);  <--------------- RenderWhenUnoccluded																		true/false (bool)

*/

/* GetTeamColor Function */
GlowStruct GetTeamColor()
{
	/* If GlowTeamColor is not valid */
	if (GlowTeamColor <= 0 || GlowTeamColor > 7)
		/* Set the default color == red */
		return CGreen;
	/* If GlowTeamColor is valid, return the selected color from the array */
	return GlowColors[GlowTeamColor - 1];
}

/* GetEnemyColor Function */
GlowStruct GetEnemyColor()
{
	/* If GlowEnemyColor is not valid */
	if (GlowEnemyColor <= 0 || GlowEnemyColor > 7)
		/* Set the default color == red */
		return CRed;
	/* If GlowEnemyColor is valid, return the selected color from the array */
	return GlowColors[GlowEnemyColor - 1];
}

/* DrawGlow Function to make our life easier */

/* Passing the GlowIndex of the current player and the Color */
void DrawGlow(int GlowIndex, GlowStruct Color)
{
	/* Take the passed GlowIndex of the current player, and the defined color and draw the Glow */
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x4)), Color.r);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x8)), Color.g);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0xC)), Color.b);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x10)), Color.a);
	Mem.Write<bool>((GlowPointer + ((GlowIndex * 0x34) + 0x24)), Color.rwo);
	Mem.Write<bool>((GlowPointer + ((GlowIndex * 0x34) + 0x25)), Color.rwuo);
}

/* Enable the Spectator Glow (also known as X-Ray) */
void GlowESP()
{
	/* Start an infinite loop */
	while (true)
	{
		/* If Glow is enabled and either ShowTeam or ShowEnemys is turned on */
		if (GlowEnabled && (GlowShowTeam || GlowShowEnemys))
		{
			/* Loop through Entitys */
			for (int g = 0; g < 64; g++)
			{
				/* Read data related to the Glow ESP */

				/* Read the current player's address in memory */
				DWORD g_currentPlayer = Mem.Read<DWORD>(Client + EntityList + ((g - 1) * 16));

				/* Read if the Entity is dormant */
				bool g_currentPlayer_Dormant = Mem.Read<bool>(g_currentPlayer + isDormant);

				/* Only read this data if the current entity is not dormant to reduce CPU usage */
				if (!g_currentPlayer_Dormant)
				{
					/* Glow Index of the current player */
					g_currentPlayer_GlowIndex = Mem.Read<int>(g_currentPlayer + GlowIndex);

					/* Read the current player's team */
					g_currentPlayer_Team = Mem.Read<int>(g_currentPlayer + m_iTeamNum);
				}

				/* Glow ESP */

				/* If current entity is not dormant && GlowShowTeam is enabled && current entity is in my team */
				if (!g_currentPlayer_Dormant && GlowShowTeam && g_currentPlayer_Team == LocalPlayer_Team)
					/* Pass the g_currentPlayer_GlowIndex and the Color through the GetTeamColor Function and draw the Glow */
					DrawGlow(g_currentPlayer_GlowIndex, GetTeamColor());
				/* If current entity is not dormant && GlowShowEnemys is enabled && current entity is in the opposing team */
				if (!g_currentPlayer_Dormant && GlowShowEnemys && g_currentPlayer_Team != LocalPlayer_Team)
					/* Pass the g_currentPlayer_GlowIndex and the Color through the GetEnemyColor Function and draw the Glow */
					DrawGlow(g_currentPlayer_GlowIndex, GetEnemyColor());

				/* Sleep to reduce CPU usage*/
				Sleep(1);
			}
		}
		else
			/* Sleep to reduce CPU usage */
			Sleep(1);
	}
}

/* ------------------- */
/* Triggerbot Function */
/* ------------------- */

/* Triggerbot Function itself */
void Triggerbot()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Check if Triggerbot is enabled && a player is in my crosshair && either Toggle ON || Key pressed && either Teamcheck ON && entity in the opposing Team || !Teamcheck */
		if ((TriggerEnabled && (inCross > 0 && inCross <= 64) && Trigger_EntityBase_TeamID != NULL) && ((TriggerToggleCheckbox && TriggerToggle) || (!TriggerToggleCheckbox && GetAsyncKeyState(TriggerKey) & 0x8000)) && ((TriggerTeamcheck && Trigger_EntityBase_TeamID != LocalPlayer_Team) || !TriggerTeamcheck))
		{
			/* If TriggerBurstCheckbox is enabled */
			if (TriggerBurstCheckbox)
				/* Burst */
				TriggerBurst();
			else
				TriggerShot();
		}
		 
		/* Sleep to reduce CPU usage */
		Sleep(1);
	}
}

/* Shoot Function */
void TriggerShot()
{
	/* TriggerDelay */
	Sleep(TriggerDelay);

	/* If TriggerUseWriteProcessMemory is enabled */
	if (TriggerUseWPM)
		/* +attack */
		Mem.Write<int>(Client + attack, 1);
	/* If TriggerUseWriteProcessMemory is not enabled (Use mouse_event) */
	else
		/* Simulate a left mouse click */
		mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);

	/* Sleep 10 ms to make sure the shot gets through */
	Sleep(10);

	/* If TriggerUseWriteProcessMemory is enabled */
	if (TriggerUseWPM)
		/* -attack */
		Mem.Write<int>(Client + attack, 0);
	/* If TriggerUseWriteProcessMemory is not enabled (Use mouse_event) */
	else
		/* Simulate a left mouse release */
		mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
}

/* Burst Function */
void TriggerBurst()
{
	for (int b = 0; b < 3; b++)
	{
		Sleep(TriggerDelay);
		TriggerShot();
		Sleep(TriggerBurstDelay);
	}
}

/* ----------------- */
/* Bunnyhop Function */
/* ----------------- */

void Bunnyhop()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Check if Bunnyhop is enabled && BunnyKey is pressed */
		if (BunnyEnabled && GetAsyncKeyState(BunnyKey))
		{
			/* Check if (jumping || jumping && walking || jumping && crouching || jumping && walking && crouching) */
			/* Don't quote me on this comment above please, this is what i think it is, never checked it */
			if (fFlag == 1 || fFlag == 3 || fFlag == 5 || fFlag == 7)
			{
				/* Check if BunnyUseWriteProcessMemory is enabled */
				if (BunnyUseWPM)
				{
					/* +jump */
					Mem.Write<int>(Client + jump, 1);
				}
				/* If BunnyUseWriteProcessMemory is not enabled (Use keybd_event) */
				else
				{
					/* Simulate a space press */
					keybd_event(VK_SPACE, 0x39, NULL, NULL);
				}

				/* Sleep sixty milliseconds */
				Sleep(50);

				/* Check if BunnyUseWriteProcessMemory is enabled */
				if (BunnyUseWPM)
				{
					/* -jump */
					Mem.Write<int>(Client + jump, 0);
				}
				/* If BunnyUseWriteProcessMemory is not enabled (Use keybd_event) */
				else
				{
					/* Simulate a space release */
					keybd_event(VK_SPACE, 0x39, KEYEVENTF_KEYUP, NULL);
				}
			}
		}

		/* Sleep to reduce CPU usage */
		Sleep(1);
	}
}

/* ----------------- */
/* No Flash Function */
/* ----------------- */

void NoFlash()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Check if NoFlash is enabled && LocalPlayer is flashed */
		if (NoFlashEnabled && FlashMaxAlpha > 0.f)
			/* Remove Flash by setting the value back to 0.f */
			Mem.Write<float>(LocalBase + flashMaxAlpha, 0.f);

		/* Sleep to reduce CPU usage */
		Sleep(1);
	}
}

/* ----- */
/* Radar */
/* ----- */

void Radar()
{
	/* Start an infinite loop */
	while (true)
	{
		/* Check if Radar is enabled */
		if (RadarEnabled)
		{
			/* Loop through Entitys */
			for (int r = 0; r < 64; r++)
			{
				/* Read data related to the Radar Hack */

				/* r_currentPlayer */
				DWORD r_currentPlayer = Mem.Read<DWORD>(Client + EntityList + ((r - 1) * 16));
				/* r_currentPlayerDormant */
				bool r_currentPlayerDormant = Mem.Read<bool>(r_currentPlayer + isDormant);
				/* r_currentPlayerbSpotted */
				bool r_currentPlayerbSpotted = Mem.Read<bool>(r_currentPlayer + bSpotted);

				/* Check if current entity is not dormant && not spotted yet */
				if (!r_currentPlayerDormant && !r_currentPlayerbSpotted)
					/* Set bSpotted to true */
					Mem.Write<bool>(r_currentPlayer + bSpotted, 1);

				/* Sleep to reduce CPU usage */
				Sleep(1);
			}
		}
		else
			/* Sleep to reduce CPU usage */
			Sleep(1);
	}
}