/* inVincible.h */

/* Includes */
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <msclr\marshal_cppstd.h>
#include "ProcMem.h"
#include "OffsetsAndVariables.h"

#pragma once

namespace inVincibleV2PublicRelease {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for inVincible
	/// </summary>
	public ref class inVincible : public System::Windows::Forms::Form
	{
	public:
		inVincible(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~inVincible()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  triggerDelayGUI;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ComboBox^  triggerKeyGUI;

	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::ComboBox^  glowEnemyGUI;

	private: System::Windows::Forms::ComboBox^  glowTeamGUI;
	private: System::Windows::Forms::CheckBox^  triggerEnabledGUI;
	private: System::Windows::Forms::CheckBox^  triggerWPMGUI;
	private: System::Windows::Forms::CheckBox^  triggerTeamCheckGUI;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::CheckBox^  glowEnabledGUI;

	private: System::Windows::Forms::CheckBox^  glowCheckEnemyGUI;

	private: System::Windows::Forms::CheckBox^  glowCheckTeamGUI;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::CheckBox^  bunnyEnabledGUI;
	private: System::Windows::Forms::CheckBox^  bunnyWPMGUI;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::ComboBox^  bunnyKeyGUI;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::CheckBox^  checkBox3;
	private: System::Windows::Forms::CheckBox^  checkBox4;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::CheckBox^  triggerToggleCheck;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::CheckBox^  bustCheckBox;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::NumericUpDown^  burstDelay;

	private: System::Windows::Forms::Label^  label20;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(inVincible::typeid));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->burstDelay = (gcnew System::Windows::Forms::NumericUpDown());
			this->bustCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->triggerToggleCheck = (gcnew System::Windows::Forms::CheckBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->triggerEnabledGUI = (gcnew System::Windows::Forms::CheckBox());
			this->triggerWPMGUI = (gcnew System::Windows::Forms::CheckBox());
			this->triggerTeamCheckGUI = (gcnew System::Windows::Forms::CheckBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->triggerDelayGUI = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->triggerKeyGUI = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->glowEnabledGUI = (gcnew System::Windows::Forms::CheckBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->glowCheckEnemyGUI = (gcnew System::Windows::Forms::CheckBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->glowCheckTeamGUI = (gcnew System::Windows::Forms::CheckBox());
			this->glowEnemyGUI = (gcnew System::Windows::Forms::ComboBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->glowTeamGUI = (gcnew System::Windows::Forms::ComboBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->bunnyEnabledGUI = (gcnew System::Windows::Forms::CheckBox());
			this->bunnyWPMGUI = (gcnew System::Windows::Forms::CheckBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->bunnyKeyGUI = (gcnew System::Windows::Forms::ComboBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->burstDelay))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->triggerDelayGUI))->BeginInit();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->burstDelay);
			this->groupBox1->Controls->Add(this->bustCheckBox);
			this->groupBox1->Controls->Add(this->label20);
			this->groupBox1->Controls->Add(this->label19);
			this->groupBox1->Controls->Add(this->triggerToggleCheck);
			this->groupBox1->Controls->Add(this->label18);
			this->groupBox1->Controls->Add(this->triggerEnabledGUI);
			this->groupBox1->Controls->Add(this->triggerWPMGUI);
			this->groupBox1->Controls->Add(this->triggerTeamCheckGUI);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->triggerDelayGUI);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->triggerKeyGUI);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(275, 162);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Triggerbot";
			// 
			// burstDelay
			// 
			this->burstDelay->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			this->burstDelay->Location = System::Drawing::Point(215, 133);
			this->burstDelay->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
			this->burstDelay->Name = L"burstDelay";
			this->burstDelay->ReadOnly = true;
			this->burstDelay->Size = System::Drawing::Size(49, 20);
			this->burstDelay->TabIndex = 18;
			this->burstDelay->ValueChanged += gcnew System::EventHandler(this, &inVincible::burstDelay_ValueChanged);
			// 
			// bustCheckBox
			// 
			this->bustCheckBox->AutoSize = true;
			this->bustCheckBox->Location = System::Drawing::Point(250, 109);
			this->bustCheckBox->Name = L"bustCheckBox";
			this->bustCheckBox->Size = System::Drawing::Size(15, 14);
			this->bustCheckBox->TabIndex = 16;
			this->bustCheckBox->UseVisualStyleBackColor = true;
			this->bustCheckBox->CheckedChanged += gcnew System::EventHandler(this, &inVincible::bustCheckBox_CheckedChanged);
			// 
			// label20
			// 
			this->label20->Location = System::Drawing::Point(143, 134);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(59, 23);
			this->label20->TabIndex = 17;
			this->label20->Text = L"Delay (ms):";
			// 
			// label19
			// 
			this->label19->Location = System::Drawing::Point(143, 108);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(91, 23);
			this->label19->TabIndex = 15;
			this->label19->Text = L"Burst:";
			// 
			// triggerToggleCheck
			// 
			this->triggerToggleCheck->AutoSize = true;
			this->triggerToggleCheck->Location = System::Drawing::Point(250, 82);
			this->triggerToggleCheck->Name = L"triggerToggleCheck";
			this->triggerToggleCheck->Size = System::Drawing::Size(15, 14);
			this->triggerToggleCheck->TabIndex = 14;
			this->triggerToggleCheck->UseVisualStyleBackColor = true;
			this->triggerToggleCheck->CheckedChanged += gcnew System::EventHandler(this, &inVincible::triggerToggleCheck_CheckedChanged);
			// 
			// label18
			// 
			this->label18->Location = System::Drawing::Point(143, 82);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(91, 23);
			this->label18->TabIndex = 13;
			this->label18->Text = L"Toggle:";
			// 
			// triggerEnabledGUI
			// 
			this->triggerEnabledGUI->AutoSize = true;
			this->triggerEnabledGUI->Location = System::Drawing::Point(108, 135);
			this->triggerEnabledGUI->Name = L"triggerEnabledGUI";
			this->triggerEnabledGUI->Size = System::Drawing::Size(15, 14);
			this->triggerEnabledGUI->TabIndex = 12;
			this->triggerEnabledGUI->UseVisualStyleBackColor = true;
			this->triggerEnabledGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::triggerEnabledGUI_CheckedChanged);
			// 
			// triggerWPMGUI
			// 
			this->triggerWPMGUI->AutoSize = true;
			this->triggerWPMGUI->Location = System::Drawing::Point(108, 109);
			this->triggerWPMGUI->Name = L"triggerWPMGUI";
			this->triggerWPMGUI->Size = System::Drawing::Size(15, 14);
			this->triggerWPMGUI->TabIndex = 11;
			this->triggerWPMGUI->UseVisualStyleBackColor = true;
			this->triggerWPMGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::triggerWPMGUI_CheckedChanged);
			// 
			// triggerTeamCheckGUI
			// 
			this->triggerTeamCheckGUI->AutoSize = true;
			this->triggerTeamCheckGUI->Location = System::Drawing::Point(108, 82);
			this->triggerTeamCheckGUI->Name = L"triggerTeamCheckGUI";
			this->triggerTeamCheckGUI->Size = System::Drawing::Size(15, 14);
			this->triggerTeamCheckGUI->TabIndex = 10;
			this->triggerTeamCheckGUI->UseVisualStyleBackColor = true;
			this->triggerTeamCheckGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::triggerTeamCheckGUI_CheckedChanged);
			// 
			// label5
			// 
			this->label5->Location = System::Drawing::Point(11, 134);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(91, 15);
			this->label5->TabIndex = 9;
			this->label5->Text = L"Enabled:";
			// 
			// label4
			// 
			this->label4->Location = System::Drawing::Point(11, 108);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(91, 23);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Use WPM:";
			// 
			// label3
			// 
			this->label3->Location = System::Drawing::Point(11, 82);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(91, 23);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Teamcheck:";
			// 
			// triggerDelayGUI
			// 
			this->triggerDelayGUI->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			this->triggerDelayGUI->Location = System::Drawing::Point(108, 53);
			this->triggerDelayGUI->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
			this->triggerDelayGUI->Name = L"triggerDelayGUI";
			this->triggerDelayGUI->ReadOnly = true;
			this->triggerDelayGUI->Size = System::Drawing::Size(157, 20);
			this->triggerDelayGUI->TabIndex = 3;
			this->triggerDelayGUI->ValueChanged += gcnew System::EventHandler(this, &inVincible::triggerDelayGUI_ValueChanged);
			// 
			// label2
			// 
			this->label2->Location = System::Drawing::Point(11, 55);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(91, 23);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Delay (ms):";
			// 
			// triggerKeyGUI
			// 
			this->triggerKeyGUI->FormattingEnabled = true;
			this->triggerKeyGUI->Items->AddRange(gcnew cli::array< System::Object^  >(63) {
				L"VK_LBUTTON", L"VK_RBUTTON", L"VK_LCONTROL",
					L"VK_RCONTROL", L"VK_LEFT", L"VK_RIGHT", L"VK_UP", L"VK_DOWN", L"VK_LMENU", L"VK_RMENU", L"VK_LSHIFT", L"VK_RSHIFT", L"VK_MBUTTON",
					L"VK_ESCAPE", L"VK_RETURN", L"VK_SPACE", L"VK_TAB", L"VK_KEY_0", L"VK_KEY_1", L"VK_KEY_2", L"VK_KEY_3", L"VK_KEY_4", L"VK_KEY_5",
					L"VK_KEY_6", L"VK_KEY_7", L"VK_KEY_8", L"VK_KEY_9", L"VK_KEY_A", L"VK_KEY_B", L"VK_KEY_C", L"VK_KEY_D", L"VK_KEY_E", L"VK_KEY_F",
					L"VK_KEY_G", L"VK_KEY_H", L"VK_KEY_I", L"VK_KEY_J", L"VK_KEY_K", L"VK_KEY_L", L"VK_KEY_M\t", L"VK_KEY_N", L"VK_KEY_O", L"VK_KEY_P",
					L"VK_KEY_Q", L"VK_KEY_R", L"VK_KEY_S", L"VK_KEY_T", L"VK_KEY_U", L"VK_KEY_V", L"VK_KEY_W", L"VK_KEY_X", L"VK_KEY_Y", L"VK_KEY_Z",
					L"VK_NUMPAD0", L"VK_NUMPAD1", L"VK_NUMPAD2", L"VK_NUMPAD3", L"VK_NUMPAD4", L"VK_NUMPAD5", L"VK_NUMPAD6", L"VK_NUMPAD7", L"VK_NUMPAD8",
					L"VK_NUMPAD9"
			});
			this->triggerKeyGUI->Location = System::Drawing::Point(108, 24);
			this->triggerKeyGUI->Name = L"triggerKeyGUI";
			this->triggerKeyGUI->Size = System::Drawing::Size(158, 21);
			this->triggerKeyGUI->TabIndex = 1;
			this->triggerKeyGUI->SelectedIndexChanged += gcnew System::EventHandler(this, &inVincible::triggerKeyGUI_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(11, 27);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(91, 23);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Key:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->glowEnabledGUI);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->glowCheckEnemyGUI);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->glowCheckTeamGUI);
			this->groupBox2->Controls->Add(this->glowEnemyGUI);
			this->groupBox2->Controls->Add(this->label8);
			this->groupBox2->Controls->Add(this->glowTeamGUI);
			this->groupBox2->Controls->Add(this->label9);
			this->groupBox2->Controls->Add(this->label10);
			this->groupBox2->Location = System::Drawing::Point(293, 12);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(273, 162);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Glow ESP";
			// 
			// glowEnabledGUI
			// 
			this->glowEnabledGUI->AutoSize = true;
			this->glowEnabledGUI->Location = System::Drawing::Point(103, 135);
			this->glowEnabledGUI->Name = L"glowEnabledGUI";
			this->glowEnabledGUI->Size = System::Drawing::Size(15, 14);
			this->glowEnabledGUI->TabIndex = 18;
			this->glowEnabledGUI->UseVisualStyleBackColor = true;
			this->glowEnabledGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::glowEnabledGUI_CheckedChanged);
			// 
			// label7
			// 
			this->label7->Location = System::Drawing::Point(6, 55);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(91, 23);
			this->label7->TabIndex = 14;
			this->label7->Text = L"Enemy Color:";
			// 
			// glowCheckEnemyGUI
			// 
			this->glowCheckEnemyGUI->AutoSize = true;
			this->glowCheckEnemyGUI->Location = System::Drawing::Point(103, 109);
			this->glowCheckEnemyGUI->Name = L"glowCheckEnemyGUI";
			this->glowCheckEnemyGUI->Size = System::Drawing::Size(15, 14);
			this->glowCheckEnemyGUI->TabIndex = 17;
			this->glowCheckEnemyGUI->UseVisualStyleBackColor = true;
			this->glowCheckEnemyGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::glowCheckEnemyGUI_CheckedChanged);
			// 
			// label6
			// 
			this->label6->Location = System::Drawing::Point(6, 27);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(91, 23);
			this->label6->TabIndex = 13;
			this->label6->Text = L"Team Color:";
			// 
			// glowCheckTeamGUI
			// 
			this->glowCheckTeamGUI->AutoSize = true;
			this->glowCheckTeamGUI->Location = System::Drawing::Point(103, 82);
			this->glowCheckTeamGUI->Name = L"glowCheckTeamGUI";
			this->glowCheckTeamGUI->Size = System::Drawing::Size(15, 14);
			this->glowCheckTeamGUI->TabIndex = 16;
			this->glowCheckTeamGUI->UseVisualStyleBackColor = true;
			this->glowCheckTeamGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::glowCheckTeamGUI_CheckedChanged);
			// 
			// glowEnemyGUI
			// 
			this->glowEnemyGUI->FormattingEnabled = true;
			this->glowEnemyGUI->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Red", L"Green", L"Blue", L"Yellow", L"Orange",
					L"Purple", L"White"
			});
			this->glowEnemyGUI->Location = System::Drawing::Point(103, 52);
			this->glowEnemyGUI->Name = L"glowEnemyGUI";
			this->glowEnemyGUI->Size = System::Drawing::Size(158, 21);
			this->glowEnemyGUI->TabIndex = 3;
			this->glowEnemyGUI->SelectedIndexChanged += gcnew System::EventHandler(this, &inVincible::glowEnemyGUI_SelectedIndexChanged);
			// 
			// label8
			// 
			this->label8->Location = System::Drawing::Point(6, 134);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(91, 15);
			this->label8->TabIndex = 15;
			this->label8->Text = L"Enabled:";
			// 
			// glowTeamGUI
			// 
			this->glowTeamGUI->FormattingEnabled = true;
			this->glowTeamGUI->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Red", L"Green", L"Blue", L"Yellow", L"Orange",
					L"Purple", L"White"
			});
			this->glowTeamGUI->Location = System::Drawing::Point(103, 24);
			this->glowTeamGUI->Name = L"glowTeamGUI";
			this->glowTeamGUI->Size = System::Drawing::Size(158, 21);
			this->glowTeamGUI->TabIndex = 2;
			this->glowTeamGUI->SelectedIndexChanged += gcnew System::EventHandler(this, &inVincible::glowTeamGUI_SelectedIndexChanged);
			// 
			// label9
			// 
			this->label9->Location = System::Drawing::Point(6, 108);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(91, 23);
			this->label9->TabIndex = 14;
			this->label9->Text = L"Show Enemys:";
			// 
			// label10
			// 
			this->label10->Location = System::Drawing::Point(6, 82);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(97, 23);
			this->label10->TabIndex = 13;
			this->label10->Text = L"Show Teammates:";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->bunnyEnabledGUI);
			this->groupBox3->Controls->Add(this->bunnyWPMGUI);
			this->groupBox3->Controls->Add(this->label11);
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->bunnyKeyGUI);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Location = System::Drawing::Point(12, 180);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(275, 109);
			this->groupBox3->TabIndex = 13;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Bunnyhop";
			// 
			// bunnyEnabledGUI
			// 
			this->bunnyEnabledGUI->AutoSize = true;
			this->bunnyEnabledGUI->Location = System::Drawing::Point(108, 80);
			this->bunnyEnabledGUI->Name = L"bunnyEnabledGUI";
			this->bunnyEnabledGUI->Size = System::Drawing::Size(15, 14);
			this->bunnyEnabledGUI->TabIndex = 12;
			this->bunnyEnabledGUI->UseVisualStyleBackColor = true;
			this->bunnyEnabledGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::bunnyEnabledGUI_CheckedChanged);
			// 
			// bunnyWPMGUI
			// 
			this->bunnyWPMGUI->AutoSize = true;
			this->bunnyWPMGUI->Location = System::Drawing::Point(108, 54);
			this->bunnyWPMGUI->Name = L"bunnyWPMGUI";
			this->bunnyWPMGUI->Size = System::Drawing::Size(15, 14);
			this->bunnyWPMGUI->TabIndex = 11;
			this->bunnyWPMGUI->UseVisualStyleBackColor = true;
			this->bunnyWPMGUI->CheckedChanged += gcnew System::EventHandler(this, &inVincible::bunnyWPMGUI_CheckedChanged);
			// 
			// label11
			// 
			this->label11->Location = System::Drawing::Point(11, 79);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(91, 15);
			this->label11->TabIndex = 9;
			this->label11->Text = L"Enabled:";
			// 
			// label12
			// 
			this->label12->Location = System::Drawing::Point(11, 53);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(91, 23);
			this->label12->TabIndex = 8;
			this->label12->Text = L"Use WPM:";
			// 
			// bunnyKeyGUI
			// 
			this->bunnyKeyGUI->FormattingEnabled = true;
			this->bunnyKeyGUI->Items->AddRange(gcnew cli::array< System::Object^  >(63) {
				L"VK_LBUTTON", L"VK_RBUTTON", L"VK_LCONTROL",
					L"VK_RCONTROL", L"VK_LEFT", L"VK_RIGHT", L"VK_UP", L"VK_DOWN", L"VK_LMENU", L"VK_RMENU", L"VK_LSHIFT", L"VK_RSHIFT", L"VK_MBUTTON",
					L"VK_ESCAPE", L"VK_RETURN", L"VK_SPACE", L"VK_TAB", L"VK_KEY_0", L"VK_KEY_1", L"VK_KEY_2", L"VK_KEY_3", L"VK_KEY_4", L"VK_KEY_5",
					L"VK_KEY_6", L"VK_KEY_7", L"VK_KEY_8", L"VK_KEY_9", L"VK_KEY_A", L"VK_KEY_B", L"VK_KEY_C", L"VK_KEY_D", L"VK_KEY_E", L"VK_KEY_F",
					L"VK_KEY_G", L"VK_KEY_H", L"VK_KEY_I", L"VK_KEY_J", L"VK_KEY_K", L"VK_KEY_L", L"VK_KEY_M\t", L"VK_KEY_N", L"VK_KEY_O", L"VK_KEY_P",
					L"VK_KEY_Q", L"VK_KEY_R", L"VK_KEY_S", L"VK_KEY_T", L"VK_KEY_U", L"VK_KEY_V", L"VK_KEY_W", L"VK_KEY_X", L"VK_KEY_Y", L"VK_KEY_Z",
					L"VK_NUMPAD0", L"VK_NUMPAD1", L"VK_NUMPAD2", L"VK_NUMPAD3", L"VK_NUMPAD4", L"VK_NUMPAD5", L"VK_NUMPAD6", L"VK_NUMPAD7", L"VK_NUMPAD8",
					L"VK_NUMPAD9"
			});
			this->bunnyKeyGUI->Location = System::Drawing::Point(108, 24);
			this->bunnyKeyGUI->Name = L"bunnyKeyGUI";
			this->bunnyKeyGUI->Size = System::Drawing::Size(158, 21);
			this->bunnyKeyGUI->TabIndex = 1;
			this->bunnyKeyGUI->SelectedIndexChanged += gcnew System::EventHandler(this, &inVincible::bunnyKeyGUI_SelectedIndexChanged);
			// 
			// label15
			// 
			this->label15->Location = System::Drawing::Point(11, 27);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(91, 23);
			this->label15->TabIndex = 0;
			this->label15->Text = L"Hold Key:";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->checkBox3);
			this->groupBox4->Controls->Add(this->checkBox4);
			this->groupBox4->Controls->Add(this->label13);
			this->groupBox4->Controls->Add(this->label14);
			this->groupBox4->Location = System::Drawing::Point(293, 180);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(275, 109);
			this->groupBox4->TabIndex = 14;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Misc";
			// 
			// checkBox3
			// 
			this->checkBox3->AutoSize = true;
			this->checkBox3->Location = System::Drawing::Point(103, 54);
			this->checkBox3->Name = L"checkBox3";
			this->checkBox3->Size = System::Drawing::Size(15, 14);
			this->checkBox3->TabIndex = 12;
			this->checkBox3->UseVisualStyleBackColor = true;
			this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &inVincible::checkBox3_CheckedChanged);
			// 
			// checkBox4
			// 
			this->checkBox4->AutoSize = true;
			this->checkBox4->Location = System::Drawing::Point(103, 28);
			this->checkBox4->Name = L"checkBox4";
			this->checkBox4->Size = System::Drawing::Size(15, 14);
			this->checkBox4->TabIndex = 11;
			this->checkBox4->UseVisualStyleBackColor = true;
			this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &inVincible::checkBox4_CheckedChanged);
			// 
			// label13
			// 
			this->label13->Location = System::Drawing::Point(6, 53);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(91, 15);
			this->label13->TabIndex = 9;
			this->label13->Text = L"Radar:";
			// 
			// label14
			// 
			this->label14->Location = System::Drawing::Point(6, 27);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(91, 23);
			this->label14->TabIndex = 8;
			this->label14->Text = L"No Flash:";
			// 
			// label16
			// 
			this->label16->Font = (gcnew System::Drawing::Font(L"Arial", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label16->Location = System::Drawing::Point(9, 292);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(181, 23);
			this->label16->TabIndex = 13;
			this->label16->Text = L"inVincible Public Release v. 2.5";
			// 
			// label17
			// 
			this->label17->Font = (gcnew System::Drawing::Font(L"Arial", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label17->Location = System::Drawing::Point(471, 292);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(97, 23);
			this->label17->TabIndex = 15;
			this->label17->Text = L"by WasserEsser";
			// 
			// inVincible
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(574, 310);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"inVincible";
			this->Text = L"inVincible Public Release V.2.5";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->burstDelay))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->triggerDelayGUI))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void triggerKeyGUI_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

		/* Super annoying convertion from String^ to std::string using the marshal library */

		System::String^ triggerKeyCLRString = (String^)(triggerKeyGUI->SelectedItem);
		std::string TriggerKeySTDString = marshal_as<std::string>(triggerKeyCLRString);

		/* Set TriggerKey based on the selected Item in the DropDownMenu (thanks Geertje123 for his suggestion) */

		/* How does it work? */

		/* We are looping through an array of items, that the DropDownMenu can contain, look which one matches the SelectedItem of triggerKeyGUI and take the i'th result and look in the list of the DWORD codes for the keys.*/

		for (int i = 0; i < sizeof(PossibleTriggerKeyText)/sizeof(PossibleTriggerKeyText[0]); i++)
		{
			if (PossibleTriggerKeyText[i] == TriggerKeySTDString)
				TriggerKey = PossibleTriggerKeyDWORD[i];
		}		
	}

private: System::Void triggerDelayGUI_ValueChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerDelay based on the Value of the Numeric Number Thingy */

	TriggerDelay = (int)triggerDelayGUI->Value;
}

private: System::Void triggerTeamCheckGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerTeamcheck on/off based on it's current state */

	TriggerTeamcheck = !TriggerTeamcheck;
}

private: System::Void triggerWPMGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerUseWPM on/off based on it's current state */

	TriggerUseWPM = !TriggerUseWPM;
}

private: System::Void triggerEnabledGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerEnabled on/off based on it's current state */

	TriggerEnabled = !TriggerEnabled;
}

private: System::Void glowTeamGUI_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
	/* Super annoying convertion from String^ to std::string using the marshal library */

	System::String^ GlowTeamCLRString = (String^)(glowTeamGUI->SelectedItem);
	std::string GlowTeamSTDString = marshal_as<std::string>((GlowTeamCLRString));

	/* Set GlowTeamColor based on the selected Item in the DropDownMenu (thanks Geertje123 for his suggestion) */

	/* How does it work? */

	/* We are looping through an array of items, that the DropDownMenu can contain, look which one matches the SelectedItem of glowTeamGUI and take the i'th result and look in the list of the DWORD codes for the keys.*/
	for (int i = 0; i < sizeof(PossibleTeamColorText) / sizeof(PossibleTeamColorText[0]); i++)
	{
		if (PossibleTeamColorText[i] == GlowTeamSTDString)
			GlowTeamColor = PossibleTeamColorInt[i];
	}
}
private: System::Void glowEnemyGUI_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Super annoying convertion from String^ to std::string using the marshal library */

	System::String^ GlowEnemyCLRString = (String^)(glowEnemyGUI->SelectedItem);
	std::string GlowEnemySTDString = marshal_as<std::string>(GlowEnemyCLRString);

	/* Set GlowEnemyColor based on the selected Item in the DropDownMenu (thanks Geertje123 for his suggestion) */

	/* How does it work? */

	/* We are looping through an array of items, that the DropDownMenu can contain, look which one matches the SelectedItem of glowEnemyGUI and take the i'th result and look in the list of the DWORD codes for the keys.*/

	for (int i = 0; i < sizeof(PossibleEnemyColorText) / sizeof(PossibleEnemyColorText[0]); i++)
	{
		if (PossibleEnemyColorText[i] == GlowEnemySTDString)
			GlowEnemyColor = PossibleEnemyColorInt[i];
	}
}
private: System::Void glowCheckTeamGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set GlowShowTeam on/off based on it's current state */

	GlowShowTeam = !GlowShowTeam;
}
private: System::Void glowCheckEnemyGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set GlowShowEnemys on/off based on it's current state */

	GlowShowEnemys = !GlowShowEnemys;
}
private: System::Void glowEnabledGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set GlowEnabled on/off based on it's current state */

	GlowEnabled = !GlowEnabled;
}

private: System::Void bunnyKeyGUI_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Super annoying convertion from String^ to std::string using the marshal library */

	System::String^ BunnyKeyCLRString = (String^)(bunnyKeyGUI->SelectedItem);
	std::string BunnyKeySTDString = marshal_as<std::string>(BunnyKeyCLRString);

	/* Set BunnyKey based on the selected Item in the DropDownMenu (thanks Geertje123 for his suggestion) */

	/* How does it work? */

	/* We are looping through an array of items, that the DropDownMenu can contain, look which one matches the SelectedItem of bunnyKeyGUI and take the i'th result and look in the list of the DWORD codes for the keys.*/

	for (int i = 0; i < sizeof(PossibleBunnyKeyText) / sizeof(PossibleBunnyKeyText[0]); i++)
	{
		if (PossibleBunnyKeyText[i] == BunnyKeySTDString)
			BunnyKey = PossibleBunnyKeyDWORD[i];
	}
}

private: System::Void bunnyWPMGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set BunnyUseWPM on/off based on it's current state */

	BunnyUseWPM = !BunnyUseWPM;
}
private: System::Void bunnyEnabledGUI_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set BunnyEnabled on/off based on it's current state */

	BunnyEnabled = !BunnyEnabled;
}

private: System::Void checkBox4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set NoFlashEnabled on/off based on it's current state */

	NoFlashEnabled = !NoFlashEnabled;
}

private: System::Void checkBox3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set RadarEnabled on/off based on it's current state */

	RadarEnabled = !RadarEnabled;
}

private: System::Void triggerToggleCheck_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerToggleCheckbox on/off based on it's current state */

	TriggerToggleCheckbox = !TriggerToggleCheckbox;
}

private: System::Void bustCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	/* Set TriggerBurstCheckbox on/off based on it's current state */

	TriggerBurstCheckbox = !TriggerBurstCheckbox;
}
private: System::Void burstDelay_ValueChanged(System::Object^  sender, System::EventArgs^  e) {

	TriggerBurstDelay = (int)burstDelay->Value;
}
};

}
